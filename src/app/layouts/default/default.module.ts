import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DefaultComponent } from './default.component';
import { SharedModule } from '../../shared/shared.module';
import { RouterModule } from '@angular/router';
import { DashboardComponent } from 'src/app/modules/dashboard/dashboard.component';
import { AuthorListeComponent } from 'src/app/modules/author/author-liste/author-liste.component';
import { AddAuthorComponent } from 'src/app/modules/author/add-author/add-author.component';
import { AddCategorieComponent } from 'src/app/modules/categorie/add-categorie/add-categorie.component';
import { CategorieListeComponent } from 'src/app/modules/categorie/categorie-liste/categorie-liste.component';
import { BookListeComponent } from 'src/app/modules/books/book-liste/book-liste.component';
import { AddBookComponent } from 'src/app/modules/books/add-book/add-book.component';
import { AddClientComponent } from 'src/app/modules/client/add-client/add-client.component';
import { ClientListeComponent } from 'src/app/modules/client/client-liste/client-liste.component';
import { DetailBookComponent } from 'src/app/modules/books/detail-book/detail-book.component';
import { DetailClientComponent } from 'src/app/modules/client/detail-client/detail-client.component';
import { DetailAuthorComponent } from 'src/app/modules/author/detail-author/detail-author.component';
import { DetailCategorieComponent } from 'src/app/modules/categorie/detail-categorie/detail-categorie.component';
import { VenteListComponent } from 'src/app/modules/vente/vente-list/vente-list.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    DefaultComponent,
    DashboardComponent,
    AuthorListeComponent,
    AddAuthorComponent,
    AddCategorieComponent,
    CategorieListeComponent,
    BookListeComponent,
    AddBookComponent,
    AddClientComponent,
    ClientListeComponent,
    DetailBookComponent,
    DetailClientComponent,
    DetailAuthorComponent,
    DetailCategorieComponent,
    VenteListComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class DefaultModule { }
