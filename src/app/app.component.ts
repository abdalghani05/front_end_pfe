import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css', '../assets/css/bootstrap.min.css', '../assets/css/dataTables.bootstrap4.min.css'],

})
export class AppComponent {
  title = 'front-end-PFE';
}
