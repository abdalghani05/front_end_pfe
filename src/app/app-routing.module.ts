import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { AuthorListeComponent } from './modules/author/author-liste/author-liste.component';
import { AddAuthorComponent } from './modules/author/add-author/add-author.component';
import { AddCategorieComponent } from './modules/categorie/add-categorie/add-categorie.component';
import { CategorieListeComponent } from './modules/categorie/categorie-liste/categorie-liste.component';
import { BookListeComponent } from './modules/books/book-liste/book-liste.component';
import { AddBookComponent } from './modules/books/add-book/add-book.component';
import { ClientListeComponent } from './modules/client/client-liste/client-liste.component';
import { AddClientComponent } from './modules/client/add-client/add-client.component';
import { VenteListComponent } from './modules/vente/vente-list/vente-list.component';
import { DashboardComponent } from './modules/dashboard/dashboard.component';
import { DefaultComponent } from './layouts/default/default.component';

const routes: Routes = [
  {path:'',component:DefaultComponent,children:[
    {path:'',component:DashboardComponent},
    { path: 'login', component: LoginComponent },
    { path: 'author-list', component: AuthorListeComponent },
    { path: 'add-author', component: AddAuthorComponent },
    { path: 'categorie-list', component: CategorieListeComponent },
    { path: 'add-categorie', component: AddCategorieComponent },
    { path: 'book-list', component: BookListeComponent },
    { path: 'add-book', component: AddBookComponent },
    { path: 'client-list', component: ClientListeComponent },
    { path: 'add-client', component: AddClientComponent },
    { path: 'Commande-list', component: VenteListComponent },
  ]},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
