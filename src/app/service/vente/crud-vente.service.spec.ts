import { TestBed } from '@angular/core/testing';

import { CrudVenteService } from './crud-vente.service';

describe('VenteService', () => {
  let service: CrudVenteService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CrudVenteService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
