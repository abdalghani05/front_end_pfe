import { TestBed } from '@angular/core/testing';

import { CrudAuthorService } from './crud-author.service';

describe('CrudAuthorService', () => {
  let service: CrudAuthorService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CrudAuthorService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
