import { TestBed } from '@angular/core/testing';

import { CrudCategorieService } from './crud-categorie.service';

describe('CrudCategorieService', () => {
  let service: CrudCategorieService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CrudCategorieService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
