import { TestBed } from '@angular/core/testing';

import { CrudBookService } from './crud-book.service';

describe('CrudBookService', () => {
  let service: CrudBookService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CrudBookService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
