export class Book {
  id!: String;
  title!: String;
  photo!:String;
  price!:Number;
  author!:Object;
  categorie!:Object;
  status !: Boolean;
}
