import { Component,OnInit } from '@angular/core';
import { CrudVenteService } from 'src/app/service/vente/crud-vente.service';

@Component({
  selector: 'app-vente-list',
  templateUrl: './vente-list.component.html',
  styleUrls: ['./vente-list.component.css']
})
export class VenteListComponent {
  Commandes: any = [];

  constructor(private crudService: CrudVenteService) { }

  ngOnInit(): void {
    this.crudService.getVentes().subscribe(res => {
      this.Commandes = res;
    })
  }
}
