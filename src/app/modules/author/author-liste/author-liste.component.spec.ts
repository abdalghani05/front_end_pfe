import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AuthorListeComponent } from './author-liste.component';

describe('AuthorListeComponent', () => {
  let component: AuthorListeComponent;
  let fixture: ComponentFixture<AuthorListeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AuthorListeComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AuthorListeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
