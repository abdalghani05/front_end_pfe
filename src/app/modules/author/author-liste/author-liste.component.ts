import { Component, OnInit } from '@angular/core';
import { CrudAuthorService } from 'src/app/service/author/crud-author.service';

@Component({
  selector: 'app-author-liste',
  templateUrl: './author-liste.component.html',
  styleUrls: ['./author-liste.component.css']
})
export class AuthorListeComponent implements OnInit {
  Authors: any = [];
  constructor(private crudService: CrudAuthorService) { }

  ngOnInit(): void {
    this.crudService.getAuthors().subscribe(res => {
      this.Authors = res;
    })
  }
  // delete(id: any, i: any) {
  //   console.log(id);
  //   this.crudService.deleteAuthor(id).subscribe(res => {
  //     this.Authors.splice(i, 1);
  //   })
  // }
}
