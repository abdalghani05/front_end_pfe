import { Component, OnInit } from '@angular/core';
import { CrudClientService } from 'src/app/service/client/crud-client.service';

@Component({
  selector: 'app-client-liste',
  templateUrl: './client-liste.component.html',
  styleUrls: ['./client-liste.component.css']
})
export class ClientListeComponent {
  Clients: any = [];

  constructor(private crudService: CrudClientService) { }

  ngOnInit(): void {
    this.crudService.getClients().subscribe(res => {
      this.Clients = res;
    })
  }
}
