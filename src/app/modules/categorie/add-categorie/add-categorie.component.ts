import { Component, OnInit, NgZone } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CrudCategorieService } from 'src/app/service/categorie/crud-categorie.service';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-add-categorie',
  templateUrl: './add-categorie.component.html',
  styleUrls: ['./add-categorie.component.css']
})
export class AddCategorieComponent implements OnInit {

  categorieForm: FormGroup;

  constructor(
    public formbuilder: FormBuilder,
    private router: Router,
    private ngZone: NgZone,
    private crudService: CrudCategorieService
  ) {
    this.categorieForm = this.formbuilder.group({
      libelle: [''],
      description: [''],
    })
  }

  ngOnInit(): void { }

  onSubmit(): any {
    this.crudService.addCategories(this.categorieForm.value).subscribe(() => {
      console.log('Data Added successfuly ...');
      this.ngZone.run(() => {
        this.router.navigateByUrl('/categorie-list')
      })
    }, (err) => {
      console.log(err);
    });
  }
}
