import { Component, OnInit } from '@angular/core';
import { CrudCategorieService } from 'src/app/service/categorie/crud-categorie.service';

@Component({
  selector: 'app-categorie-liste',
  templateUrl: './categorie-liste.component.html',
  styleUrls: ['./categorie-liste.component.css']
})
export class CategorieListeComponent {

  Categories: any = [];

  constructor(private crudService: CrudCategorieService) { }

  ngOnInit(): void {
    this.crudService.getCategories().subscribe(res => {
      this.Categories = res;
    })
  }

}
