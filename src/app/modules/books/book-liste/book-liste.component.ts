import { Component, OnInit } from '@angular/core';
import { CrudBookService } from 'src/app/service/book/crud-book.service';


@Component({
  selector: 'app-book-liste',
  templateUrl: './book-liste.component.html',
  styleUrls: ['./book-liste.component.css']
})
export class BookListeComponent {
  Books: any = [];

  constructor(private crudService: CrudBookService) { }

  ngOnInit(): void {
    this.crudService.getBooks().subscribe(res => {
      this.Books = res;
    })
  }
}
