import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BookListeComponent } from './book-liste.component';

describe('BookListeComponent', () => {
  let component: BookListeComponent;
  let fixture: ComponentFixture<BookListeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BookListeComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BookListeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
